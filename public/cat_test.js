let productList = document.querySelector('.product-list')
// let productTemplate = productList.querySelector('.product')
let listCatSnack = document.getElementById('catSnack')
let listCatCleansingTool = document.getElementById('catCleansingTool')
let listCatToy = document.getElementById('catToy')
let listCatBrand3 = document.getElementById('catBrand3')
let listCatBrand4 = document.getElementById('catBrand4')
let listCatBrand5 = document.getElementById('catBrand5')
let listCatPage = document.getElementById('catPage')
let listCatBrand6 = document.getElementById('catBrand6')


function cartProduct(id){
    // finished version! LAST UPDATE AT 13/5/2022 BY PAUL
    
    console.log("Product ID: ", id, " added to cart...")

    var retrievedCart = localStorage.getItem('cartLocal');
                    
            if(retrievedCart === null || retrievedCart === undefined) {
                // new item added to cart(in local storage)
                var newItem = [{"productId": parseInt(id), "cart_qty": 1}];
                newItemString = JSON.stringify(newItem)
                
                localStorage.setItem('cartLocal', newItemString);
                
                newItemJson = JSON.parse(newItemString)
                retrievedCart = newItemJson;
                console.log("new item created")
            } 
            else {
                let ExistedCart = JSON.parse(retrievedCart);
                //{"productId": parseInt(id), "cart_qty": 1}

                let found = false;
                for(let i in ExistedCart){
                    console.log(i);
                    if(ExistedCart[i].productId == id){
                        ExistedCart[i].cart_qty ++;
                        found = true;
                        break;
                    }
                }

                if(!found){
                    ExistedCart.push({"productId": id, "cart_qty": 1})
                }

                localStorage.setItem('cartLocal', JSON.stringify(ExistedCart))
            }           
}

fetch('/product')
    .then(res => res.json())
    .catch(error => ({ error: String(error) }))
    .then(json => {
        if (json.error) {
            Swal.fire({
                icon: 'error',
                title: 'Failed to load products',
                text: json.error,
            })
            return
        }
        json.forEach(showProduct)
    })

function removeProductFromDOM(id) {
    let product = productList.querySelector(`.product[data-id="${id}"]`)
    if (product) {
        product.remove()
    }
}

function removeProductListFromDOM() {
    let products = productList.querySelectorAll('.product')
    
    products.forEach( product => {
        product.remove()
    })        
    // why 4 products each time to remove?
}

let isAutoPush = false

function showProduct(product) {
    // console.log(product)
    // let productContainer
    // if (isAutoPush) {
    //     // show this product at top
    //     removeProductFromDOM(product.id)
    //     // console.log("removedProduct-id to clone for new")
    //     productContainer = productTemplate.cloneNode(true)
    // } else {
    //     // show this product in original order
    //     productContainer = productList.querySelector(`.product[data-id="${product.id}"]`)
    //     if (productContainer) {
    //         product.mode = 'in-place'
    //     } else {
    //         productContainer = productTemplate.cloneNode(true)
    //     }
    // }

    // productContainer.dataset.id = product.id 

    // // let memoLink = memoContainer.querySelector('.memo-detail-link')
    // // memoLink.href = '/memo.html?id=' + memo.id

    // let productContent = productContainer.querySelector('.product-content')
    // productContent.innerHTML = product.product_name + productContent.innerHTML;

    // let img = productContainer.querySelector('img')
    // // console.log(productContent.textContent)
    // if (product.product_image) {
    //     img.src = "img/" + product.product_image
    // } else {
    //     img.remove()
    // }

    // let productSmall = productContainer.querySelector('small')
    // productSmall.innerHTML = " Price: HKD " + product.price + productSmall.innerHTML;

    // productList.appendChild(productContainer)


    const itemBox = `
    <div class="product" data-id="${product.id}">
    <div class="product-inner">
        <div class="product-content">
            <img src="img/${product.product_image || ""}" class="d-block mx-auto" />
            <h3>${product.product_name || ""}</h3>
            <div class="bar-row">
                <small>Price: HKD ${product.price}</small>
                    <!-- <button class="like-btn" onclick="likeMemo()">💓</button> -->
                    <button onclick="likeProduct()"><i class="fa-solid fa-tag"></i>&nbsp Save to wishlist</button>
                    <!-- <button class="unlike-btn" onclick="unlikeMemo()">💔</button> -->
                <button action="addToCart" onclick="cartProduct(${product.id})">Add to Cart</button>
            </div>
        </div>
    </div>
    </div>
    `


    productList.innerHTML += itemBox;

}
// <---------------------------------------------------------------->

listCatPage.addEventListener('click', event => {
    event.preventDefault()
    // console.log("clicked catPage")
    fetch('/catPage')
        .then(res => res.json())
        .catch(error => ({ error: String(error) }))
        .then(json => {
            if (json.error) {
                Swal.fire({
                    icon: 'error',
                    title: 'Failed to load product catPage',
                    text: json.error,
                })
                return
            }
            json.forEach(removeProductListFromDOM)
            // console.log("removed Whole List")
            json.forEach(showProduct)
            // console.log("loaded catPage")
        })

})

listCatSnack.addEventListener('click', event => {
    event.preventDefault()
    // console.log("clicked catSnack")
    fetch('/catSnack')
        .then(res => res.json())
        .catch(error => ({ error: String(error) }))
        .then(json => {
            if (json.error) {
                Swal.fire({
                    icon: 'error',
                    title: 'Failed to load product catSnack',
                    text: json.error,
                })
                return
            }
            json.forEach(removeProductListFromDOM)
            // console.log("removed Whole List")
            json.forEach(showProduct)
            // console.log("loaded catSnack")
        })

})

listCatCleansingTool.addEventListener('click', event => {
    event.preventDefault()
    // console.log("clicked catCleansingTool")
    fetch('/catCleansingTool')
        .then(res => res.json())
        .catch(error => ({ error: String(error) }))
        .then(json => {
            if (json.error) {
                Swal.fire({
                    icon: 'error',
                    title: 'Failed to load product catCleansingTool',
                    text: json.error,
                })
                return
            }
            json.forEach(removeProductListFromDOM)
            // console.log("removed Whole List")
            json.forEach(showProduct)
            // console.log("loaded catCleansingTool")
        })

})

listCatToy.addEventListener('click', event => {
    event.preventDefault()
    // console.log("clicked catToy")
    fetch('/catToy')
        .then(res => res.json())
        .catch(error => ({ error: String(error) }))
        .then(json => {
            if (json.error) {
                Swal.fire({
                    icon: 'error',
                    title: 'Failed to load product catToy',
                    text: json.error,
                })
                return
            }
            json.forEach(removeProductListFromDOM)
            // console.log("removed Whole List")
            json.forEach(showProduct)
            // console.log("loaded catToy")
        })

})

listCatBrand3.addEventListener('click', event => {
    event.preventDefault()
    // console.log("clicked catBrand")
    fetch('/catBrand3')
        .then(res => res.json())
        .catch(error => ({ error: String(error) }))
        .then(json => {
            if (json.error) {
                Swal.fire({
                    icon: 'error',
                    title: 'Failed to load product catBrand',
                    text: json.error,
                })
                return
            }
            json.forEach(removeProductListFromDOM)
            // console.log("removed Whole List")
            json.forEach(showProduct)
            // console.log("loaded catBrand")
        })

})

listCatBrand4.addEventListener('click', event => {
    event.preventDefault()
    // console.log("clicked catBrand")
    fetch('/catBrand4')
        .then(res => res.json())
        .catch(error => ({ error: String(error) }))
        .then(json => {
            if (json.error) {
                Swal.fire({
                    icon: 'error',
                    title: 'Failed to load product catBrand',
                    text: json.error,
                })
                return
            }
            json.forEach(removeProductListFromDOM)
            // console.log("removed Whole List")
            json.forEach(showProduct)
            // console.log("loaded catBrand")
        })

})

listCatBrand5.addEventListener('click', event => {
    event.preventDefault()
    // console.log("clicked catBrand")
    fetch('/catBrand5')
        .then(res => res.json())
        .catch(error => ({ error: String(error) }))
        .then(json => {
            if (json.error) {
                Swal.fire({
                    icon: 'error',
                    title: 'Failed to load product catBrand',
                    text: json.error,
                })
                return
            }
            json.forEach(removeProductListFromDOM)
            // console.log("removed Whole List")
            json.forEach(showProduct)
            // console.log("loaded catBrand")
        })

})

listCatBrand6.addEventListener('click', event => {
    event.preventDefault()
    // console.log("clicked catBrand")
    fetch('/catBrand6')
        .then(res => res.json())
        .catch(error => ({ error: String(error) }))
        .then(json => {
            if (json.error) {
                Swal.fire({
                    icon: 'error',
                    title: 'Failed to load product catBrand',
                    text: json.error,
                })
                return
            }
            json.forEach(removeProductListFromDOM)
            // console.log("removed Whole List")
            json.forEach(showProduct)
            // console.log("loaded catBrand")
        })

})


// var x = document.getElementById("data-id").parentNode;
