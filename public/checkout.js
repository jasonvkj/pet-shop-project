// const { isConstructorDeclaration } = require("typescript")

// Example starter JavaScript for disabling form submissions if there are invalid fields
// (function () {
//     'use strict'

//     // Fetch all the forms we want to apply custom Bootstrap validation styles to
//     var forms = document.querySelectorAll('.needs-validation')

//     // Loop over them and prevent submission
//     Array.prototype.slice.call(forms)
//         .forEach(function (form) {
//             form.addEventListener('submit', function (event) {
//                 if (!form.checkValidity()) {
//                     event.preventDefault()
//                     event.stopPropagation()
//                 }

//                 form.classList.add('was-validated')
//             }, false)
//         })
// })()

let cartItemBox = `
    <li class="list-group-item d-flex justify-content-between lh-sm">
    <div>
    <h6 class="my-0">Product name</h6>
    <small class="text-muted">Brief description</small>
    </div>
    <span class="text-muted">$12</span>
    </li>
    `
// let 



// ------------------------------PAYMENT----------------------------------

const checkoutBtn = document.querySelector("#checkoutBtn")



checkoutBtn.addEventListener("click", (event) => {
  // let session = fetch(`/session`, {
  //   method:"GET"
  // })
  // .then(res => res.json())
  // .catch(err => console.error(err))

  // let user = session.json()
  // // console.log(JSON.stringify(user));
  // console.log(user);


  fetch("https://pet-shop.loca.lt/create-checkout-session", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      items: JSON.stringify(localStorage.getItem('cartLocal'))
    }),
  })
  
    // .then(res => res.json())
    // .catch(error => ({error: String(error)}))
    // .then( json => {if (json.error){
    //   // TODO: handle error
    // }else {
    //   // TODO: handle error
    // }})

    .then(res => {
      if (res.ok) return res.json()
      return res.json().then(json => Promise.reject(json))
    })
    .then(({ url }) => {
        // console.log(url)
      window.location = url
    })
    .catch(err => {
      console.error(err)
    })

    event.preventDefault();
})

// let checkoutBoxSelector = document.querySelector('#checkout-form')
// let infoBoxSelector = document.querySelector('.infobox')
let sumBoxContainer = document.querySelector('.cart-container')
let sumBoxItem = document.querySelector('.cart-item')

// infoBoxSelector.remove()
sumBoxItem.remove()

// let infoNode = infoBoxSelector.cloneNode(true)

cartData = JSON.parse(localStorage.getItem("cartLocal"))
let totalAmount = 0;
cartData.forEach(async (item) => {
  let sumBoxNode = sumBoxItem.cloneNode(true)

  let res = await fetch(`/product/${item.productId}`)
  let product = await res.json()

  console.log(product.product_name)
  sumBoxNode.querySelector('.name').textContent = product.product_name
  sumBoxNode.querySelector('.qtydes').textContent = "Qty: " + item.cart_qty
  sumBoxNode.querySelector('.price').textContent = "$" + product.price

  sumBoxContainer.appendChild(sumBoxNode)

  totalAmount += (product.price * item.cart_qty)
  document.querySelector('#total').innerHTML = `$` + totalAmount
  document.querySelector('.badge').textContent = cartData.length + 1
})


window.onload = async function(){
  console.log('test')
  let a = await fetch('/finduserdata')
  let b = await a.json()

  console.log (b['sqlResult'].first_name)
  console.log (b['sqlResult'].last_name)
  console.log (b['sqlResult'].email)
  console.log (b['sqlResult'].shipping_address)

  document.querySelector('#firstName').placeholder = b['sqlResult'].first_name
  document.querySelector('#firstName').value = b['sqlResult'].first_name
  document.querySelector('#lastName').placeholder = b['sqlResult'].last_name
  document.querySelector('#lastName').value = b['sqlResult'].last_name
  document.querySelector('#email').placeholder = b['sqlResult'].email
  document.querySelector('#email').value = b['sqlResult'].email
  document.querySelector('#address').placeholder = b['sqlResult'].shipping_address
  document.querySelector('#address').value = b['sqlResult'].shipping_address
}

// checkoutBoxSelector.appendChild(infoNode)
