let currentUser = document.querySelector("#current-username");
let loginForm = document.querySelector("#login-form");
let logoutForm = document.querySelector("#logout-form");
let signupForm = document.querySelector("#signup-form");

let accCheck = document.querySelector("#account-check");

let user_id = null;

fetch("/session")
  .then((res) => res.json())
  .catch((error) => ({ error: String(error) }))
  .then((json) => {
    if (json.error) {
      let message = "Error, please try again.";
      console.error(message, json.error);
    }
    setUserId(json.id);
    currentUser.textContent = json.username;
  });

function addUserStyle() {
  let link = document.createElement("link");
  link.id = "user-style";
  link.rel = "stylesheet";
  link.href = "/user/user.css";
  document.head.appendChild(link);
}

function removeUserStyle() {
  let link = document.querySelector("#user-style");
  if (link) {
    link.remove();
  }
}

function loadAdminStyle() {
  let link = document.createElement("link");
  link.id = "admin-style";
  link.rel = "stylesheet";
  link.href = "/admin/admin.css";
  document.head.appendChild(link);
}
function unloadAdminStyle() {
  let link = document.querySelector("#admin-style");
  if (link) {
    link.remove();
  }
}

function ajaxForm(options) {
  const { form, getBody, cb } = options;
  form.addEventListener("submit", (event) => {
    event.preventDefault();
    Promise.resolve(getBody)
      .then((getBody) => JSON.stringify(getBody()))
      .then((body) =>
        fetch(form.action, {
          method: form.method,
          headers: {
            "Content-Type": "application/json",
          },
          body,
        })
      )
      .then((res) => res.json())
      .catch((error) => ({ error: String(error) }))
      .then(cb);
  });
}

ajaxForm({
  form: loginForm,
  getBody() {
    return {
      username: loginForm.username.value,
      password: loginForm.password.value,
    };
  },
  cb: (json) => {
    if (json.error) {
      // console.log(value)
      console.error("Error login Ajax Form: ", json.error);
      return;
    }
    // console.log(typeof json.id)
    setUserId(json.id);
    currentUser.textContent = loginForm.username.value;
    window.location.href = "/";
  },
});

ajaxForm({
  form: signupForm,
  getBody() {
    if (signupForm.password.value !== signupForm.password2.value) {
      throw "password not matched";
    }
    return {
      username: signupForm.username.value,
      password: signupForm.password.value,
    };
  },
  cb: (json) => {
    console.log(json);
    if (json.error) {
      // console.log(value)
      console.error("Error signup Ajax Form: ", json.error);
      return;
    }
    if (json.message) {
      console.log(json.message);
      accCheck.textContent = json.message;
      return;
    }
    // console.log(typeof json.id)
    setUserId(json.id);
    currentUser.textContent = signupForm.username.value;
    window.location.href = "./myaccount.html";
  },
});

ajaxForm({
  form: logoutForm,
  getBody() {
    return {};
  },
  cb: (json) => {
    if (json.error) {
      console.error("Error log out Ajax Form: " + json.error);
      return;
    }
    setUserId(null);
  },
});

function setUserId(id) {
  user_id = id;
  if (user_id) {
    addUserStyle();
  } else {
    removeUserStyle();
  }
  // reconnectSocketIO()
}
