let checkoutBtn = document.querySelector('#checkoutBtn')


testCartJsonData = [
    {
    "product_id": 8,
    "cart_qty": 5,
    },

    {
    "product_id": 2,
    "cart_qty": 2,
    }

];

// let clickedProductId = 0;

// localStorage.setItem("cartLocal", JSON.stringify(testCartJsonData))
// testCartJsonData = JSON.parse(localStorage.getItem("cartLocal"))
// testCartJsonData = JSON.parse(localStorage.setItem("cartLocal"))
// localStorage.setItem("cartLocal", JSON.stringify(testCartJsonData))
testCartJsonData = JSON.parse(localStorage.getItem("cartLocal"))
// console.log(testCartJsonData);
// window.addEventListener("storage", function (event){
//     console.log(event.newValue, clickedProductId);
// })

let cartTotal = 0;
var clicked = false;

 let cartList = document.querySelector('.cartList')
   let itemTemplate = document.querySelector('.product_item');
   itemTemplate.remove()

  let items = testCartJsonData;

  items.forEach(async (item, index) => {
    let itemNode = itemTemplate.cloneNode(true)
    let res = await fetch(`/product/${item.productId}`)
    let product = await res.json()

    let productCount = item.cart_qty;

    itemNode.querySelector('.product_name').textContent = product.product_name
    itemNode.querySelector('#product_img').src = "img/" + product.product_image
    itemNode.querySelector('#itemval').textContent = product.price
    itemNode.querySelector('#textbox').value = productCount
  

    // Cart Total Amount & Shipping Calculation
    // var shipping_charge = document.getElementById('shipping_charge');
    // console.log(shipping_charge.innerHTML)

    cartTotal += product.price * productCount;
    document.querySelector('#product_total_amt').innerHTML = parseInt(cartTotal)
    document.querySelector('#total_cart_amt').innerHTML = parseInt(cartTotal)
    // parseInt(shipping_charge.innerHTML)

    //Change Quantity and Total Amount
        // increase item quantity
    itemNode.querySelector('[data-action="increase-item"]').addEventListener("click", function(){
        if (productCount < 99) {
        var newProductTotal = parseInt(document.querySelector('#product_total_amt').innerHTML)
        var newTotal = parseInt(document.querySelector('#total_cart_amt').innerHTML)

        productCount = productCount + 1;
        console.log('clicked', product.product_name, productCount)
        itemNode.querySelector('#textbox').value = productCount
        document.querySelector('#product_total_amt').innerHTML = newProductTotal + parseInt(product.price)
        document.querySelector('#total_cart_amt').innerHTML = newTotal + parseInt(product.price)
        
        updateCart(item.productId, productCount)
        console.log(item.productId, productCount)

        }
    });
        // decrease item quantity
    itemNode.querySelector('[data-action="decrease-item"]').addEventListener("click", function(){
        if (productCount > 1) {
        var newProductTotal = parseInt(document.querySelector('#product_total_amt').innerHTML)
        var newTotal = parseInt(document.querySelector('#total_cart_amt').innerHTML)

        productCount = productCount - 1; 
        console.log('clicked', product.product_name, productCount)
        itemNode.querySelector('#textbox').value = productCount
        document.querySelector('#product_total_amt').innerHTML = newProductTotal - parseInt(product.price)
        document.querySelector('#total_cart_amt').innerHTML = newTotal - parseInt(product.price)
        
        updateCart(item.productId, productCount)
        console.log(item.productId, productCount)
        }
    });


    itemNode.querySelector('[data-action="remove"]').addEventListener('click', function () {
        let oldCart = JSON.parse(localStorage.getItem('cartLocal'));
        let newCart = oldCart.filter( el => el.productId !== item.productId );
        localStorage.setItem('cartLocal', JSON.stringify(newCart))
        console.log(
            'old cart: ' + JSON.stringify(oldCart),
            'new cart: ' + JSON.stringify(newCart)
            )
        itemNode.remove();
    })

    itemNode.querySelector('[data-action="wishlist"]').addEventListener('click', function () {
        addToWishList(item.productId);
    })

    // pushing products into the cart list with unique html id attribute
    var showCartItem = cartList.appendChild(itemNode)
    showCartItem.id = "CartProduct" + (index + 1)
  })


  
// Put the cart data into the localStorage (json object to string) 
var cartLocal = testCartJsonData
// localStorage.setItem('cartLocal', JSON.stringify(cartLocal));

function updateCart(id, newQty){

    // --- BEENO ---
    // let key = id+":cart"
    // localStorage.setItem(key, newQty);

    // Retrieve the cart data from localStorage (string to json object)
    var retrievedCart = localStorage.getItem('cartLocal');
    var jsonRetrievedCart = JSON.parse(retrievedCart)

    // verify and change the data retrieved from localStorage
    jsonRetrievedCart.forEach( (element) => {
        if (element["productId"] == id) {
            console.log("before edit: " + element["cart_qty"])
            element["cart_qty"] = newQty
            console.log("after edit: " + element["cart_qty"])
            console.log(jsonRetrievedCart)
        }
    })

    // Update the cart data to localStorage
    var editedCartJson = jsonRetrievedCart
    console.log(editedCartJson)
    localStorage.setItem('cartLocal', JSON.stringify(editedCartJson))
    return;

}


// <-------wish-list function------->
function happySave() {
    if (!clicked) {
        clicked = true;
        Swal.fire({
            position: 'center',
            // icon: 'success',
            // background: 'yellow',
            title: 'Saved in your wish list🤗',
            showConfirmButton: false,
            timer: 1100
          })
        clicked = false;
    }
}

function addToWishList(id) {
    fetch(`/product/${id}/like`, { method: 'POST' })
        .then(res => res.json())
        .catch(err => ({ error: String(err) }))
        .then(json => {
            if (json.error) {
                // alert(json.error) // this can be blocked

                Swal.fire({
                    icon: 'error',
                    title: 'Log-in is required.',
                    text: json.error,
                })
                return
            }
            //   errorMessage.hidden = true
            happySave();
            console.log("Show like-reminder")
                })
    console.log("Added to wishList successfully")
}


// CHECKOUT VERIFICATION
checkoutBtn.addEventListener('click', async function() {
    let checkUser = await fetch('/checkuser')
    let checkUserResult = await checkUser.json()

    if(checkUserResult.login == true){
        location.href='./checkout.html'
    }else if(checkUserResult.login == false){
        Swal.fire({
            icon: 'error',
            title: 'Log-in is required.',
            // text: json.error,
        
        })
        // Swal.fire({
        //     title: 'Login Form',
        //     html: `<input type="text" id="loginSwal" class="swal2-input" placeholder="Username">
        //     <input type="password" id="passwordSwal" class="swal2-input" placeholder="Password">
        //     <div id="login-form">
        //     <a href="/connect/google">Login via Google</a>
        //     <a href="./login.html" id="register-button">Register</a>
        //     </div>
        //     `,
        //     confirmButtonText: 'Sign in',
        //     focusConfirm: false,
        //     preConfirm: () => {
        //       const login = Swal.getPopup().querySelector('#loginSwal').value
        //       const password = Swal.getPopup().querySelector('#passwordSwal').value
        //       if (!login || !password) {
        //         Swal.showValidationMessage(`Please enter login and password`)
        //       }
        //       return { login: login, password: password }
        //     }
        //   }).then((result) => {
        //     Swal.fire(`
        //       Login: ${result.value.login}
        //       Password: ${result.value.password}
        //     `.trim())
        //   })
        return
    }
})

