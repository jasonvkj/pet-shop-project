let orderNo = document.querySelector('.list-group1')
let productHistoryList = document.querySelector('.productHistory-list')
let orderHistoryList = document.getElementById('orderInfo')
let userInfoList = document.getElementById('userInfo')
let totalamountContainer = document.getElementById('totalamount')
let listGroupItems = document.querySelectorAll('.list-group-item')


// <-------looping OrderbyOtherSortingMethods------------>
// fetch('/orderNo/id')
//     .then(res => res.json())
//     .catch(error => ({ error: String(error) }))
//     .then(json => {
//         if (json.error) {
//             Swal.fire({
//                 icon: 'error',
//                 title: 'Failed to load order.status',
//                 text: json.error,
//             })
//             return
//         }
//         json.forEach(showOrderStatus)
//     })

// function showOrderStatus(order) {
//     console.log(order)
//     const itemBoxOrderStatus = `
//     <ul class="list-group">
//     <li class="list-group-item" type="button" data-id="${order.id}">${order.status || ""}</li>
//     </ul>
//         `
//     //productType_name should be producttype_name per what's shown on website console.log    
//     orderStatus.innerHTML += itemBoxOrderStatus;

// }

fetch('/orderNo/id')
    .then(res => res.json())
    .catch(error => ({ error: String(error) }))
    .then(json => {
        if (json.error) {
            Swal.fire({
                icon: 'error',
                title: 'Failed to load order Date',
                text: json.error,
            })
            return
        }
        // json.forEach(showOrderDate)
    })

// function showOrderDate(order) {
//     console.log(order)
//     let orderDate = new Date(order.created_at.replace(' ', 'T'));
//     let orderDateYMD = orderDate.toISOString().split('T')[0]
//     console.log("orderDate in yyyy/mm/dd: ", orderDateYMD) 

//     const itemBoxOrderDATE = `
//     <ul class="list-group">
//     <li class="list-group-item" type="button" onclick="selectOrderedProductByTimestamp(${order.created_at})" data-id="${order.id}">${order.created_at || ""}</li>
//     </ul>
//         `
//     //productType_name should be producttype_name per what's shown on website console.log    
//     orderDATE.innerHTML += itemBoxOrderDATE;
//     console.log(itemBoxOrderDATE)
// }

// <--------------Listing ordered items after clicking on orderNo------->

// fetch(`/listOrderedItem/id`)
//     .then(res => res.json())
//     .catch(error => ({ error: String(error) }))
//     .then(json => {
//         if (json.error) {
//             Swal.fire({
//                 icon: 'error',
//                 title: 'Log-in is required.',
//                 text: json.error,
//             })
//             return
//         }
//         json.forEach(showProduct)
//     })

// function removeProductFromDOM(id) {
//     let productHistory = productList.querySelector(`.product[data-id="${id}"]`)
//     if (productHistory) {
//         productHistory.remove()
//     }
// }

// function removeProductListFromDOM() {
//     let productHistories = productList.querySelectorAll('.product')

//     productHistories.forEach(product => {
//         productHistory.remove()
//     })
// }

// let isAutoPush = false

// function showProduct(productHistory) {
//     console.log(productHistory)

//     const itemBox = `
//     <div class="product" data-id="${productHistory.id}">
//     <div class="product-inner">
//         <div class="product-content">
//         <h6>Name: ${productHistory.product_name || ""}</h6>
//             <div class="bar-row">
//                 <small>Qty: ${productHistory.quantity}</small>
//                 <small>Price: HKD ${productHistory.product_price}</small>
//                 <small>Price: HKD ${productHistory.price_total}</small>
//             </div>
//         </div>
//     </div>
//     </div>
//     `

//     productList.innerHTML += itemBox;

// }

// function selectOrderedProduct(id) {
//     fetch(`/listOrderedItem/${id}`)
//         .then(res => res.json())
//         .catch(error => ({ error: String(error) }))
//         .then(json => {
//             if (json.error) {
//                 Swal.fire({
//                     icon: 'error',
//                     title: 'Failed to load orderedproducts',
//                     text: json.error,
//                 })
//                 return
//             }
//             json.forEach(removeProductListFromDOM)
//             // console.log("removed Whole List")
//             json.forEach(showProduct)
//             console.log("loaded brand product")
//         })

//     // console.log("Added to wishList successfully")
// }


// <-------------looping OrderNo---------------->

fetch('/orderNo/id')
    .then(res => res.json())
    .catch(error => ({ error: String(error) }))
    .then(json => {
        if (json.error) {
            Swal.fire({
                icon: 'error',
                title: 'Failed to load order.No',
                text: json.error,
            })
            return
        }
        json.forEach(showOrderNo)
    })

function showOrderNo(order) {
    console.log(order.id)
    const itemBoxOrderNo = `
    <ul class="list-group">
    <li class="list-group-item" type="button" onclick="selectOrderedProduct(${order.id})" data-id="${order.id}">Order.No#${order.id || ""}</li>
    </ul>
        `
    //productType_name should be producttype_name per what's shown on website console.log    
    orderNo.innerHTML += itemBoxOrderNo;

    // console.log(order.at(-1).id)
    // selectOrderedProduct(order.id)
}

// <------------------------SelectingEachOrderNoToShowDetails-------------->

function selectOrderedProduct(id) {

    // var shipping_charge = document.getElementById('shipping_charge');
    // var eachTotal = document.getElementById('price_total');
    // subtotal += eachTotal.innerHTML
    // document.querySelector('#product_total_amt').innerHTML = parseInt(subtotal)
    // document.querySelector('#total_cart_amt').innerHTML = 100 + parseInt(shipping_charge.innerHTML)

    fetch(`/order/${id}`)
        .then(res => res.json())
        .catch(error => ({ error: String(error) }))
        .then(json => {
            if (json.error) {
                Swal.fire({
                    icon: 'error',
                    title: 'Failed to load productHistory',
                    text: json.error,
                })
                return
            }

            fetch(`/order/${id}/orderInfo`)
                .then(res => res.json())
                .catch(error => ({ error: String(error) }))
                .then(json => {
                    if (json.error) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Failed to load productHistory',
                            text: json.error,
                        })
                        return
                    }

                    fetch(`/listUserInfo/${id}`)
                        .then(res => res.json())
                        .catch(error => ({ error: String(error) }))
                        .then(json => {
                            if (json.error) {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Failed to load users info',
                                    text: json.error,
                                })
                                return
                            }

                            json.forEach(removeUserInfoFromDOM)
                            json.forEach(showUserInfo)

                        })

                    json.forEach(removeOrderInfoFromDOM)
                    json.forEach(showOrderInfo)
                })

            json.forEach(removeProductListFromDOM)
            // console.log("removed Whole List")
            json.forEach(showProductHistory)
            console.log("loaded details")
        })
    // console.log("")

}


// function removeProductFromDOM(id) {
//     let productHistory = productHistoryList.querySelector(`.product[data-id="${id}"]`)
//     if (productHistory) {
//         productHistory.remove()
//     }
// }

function removeProductListFromDOM() {
    productHistoryList.innerHTML = ""
    // let productHistories = productHistoryList.querySelectorAll('.product')

    // productHistories.forEach(product => {
    //     productHistory.remove()
    // })
}

// let isAutoPush = false

function showProductHistory(productHistory) {
    console.log(productHistory)

    const itemBox = `
<div class="row mb-2 mb-sm-0 py-25 bgc-default-l4">
    <div class="d-none d-sm-block col-1">#</div>
    <div class="col-9 col-sm-5">${productHistory.product_name || ""}</div>
    <div class="d-none d-sm-block col-2">${productHistory.quantity}</div>
    <div class="d-none d-sm-block col-2 text-95">${productHistory.product_price}</div>
    <div class="col-2 text-secondary-d2" id="price_total">${productHistory.price_total}</div>
</div> 
    `
    // <div class="product" data-id="${productHistory.id}">
    // <div class="product-inner">
    //     <div class="productHistory-content">
    //         <div class="bar-row">
    //         <small>Product Name ${productHistory.product_name || ""}</small>
    //             <small>Unit Price: HKD ${productHistory.product_price}</small>
    //             <small>Qty: ${productHistory.quantity}</small>
    //             <small>Total Price: HKD ${productHistory.price_total}</small>
    //         </div>
    //     </div>
    // </div>
    // </div>


    productHistoryList.innerHTML += itemBox;   

}

function showOrderInfo(order) {
    console.log(order)
    let orderDate = new Date(order.created_at.replace(' ', 'T'));
    let orderDateYMD = orderDate.toISOString().split('T')[0]
    // console.log("orderDate in yyyy/mm/dd: ", orderDateYMD)

    const itemBoxOrderInfo = `
<div class="mt-1 mb-2 text-secondary-m1 text-600 text-125">
Invoice
</div>

<div class="my-2"><i class="fa fa-circle text-blue-m2 text-xs mr-1"></i> <span class="text-600 text-90">ID:</span> ${order.id}</div>

<div class="my-2"><i class="fa fa-circle text-blue-m2 text-xs mr-1"></i> <span class="text-600 text-90">Issue Date:</span> ${orderDateYMD}</div>

<div class="my-2"><i class="fa fa-circle text-blue-m2 text-xs mr-1"></i> <span class="text-600 text-90">Status:</span> <span class="badge badge-warning badge-pill px-25">${order.status}</span></div> 
`
    orderHistoryList.innerHTML = itemBoxOrderInfo;

    totalAmount = order.order_total
    console.log(totalAmount)
    const totoalAmountItemBox = `
    <div class="row mt-3">
    <div class="col-12 col-sm-7 text-grey-d2 text-95 mt-2 mt-lg-0">
        Thank you for your business. Happy shopping with US!
    </div>

    <div class="col-12 col-sm-5 text-grey text-90 order-first order-sm-last">
        <div class="row my-2">
            <div class="col-7 text-right">
                SubTotal
            </div>
            <div class="col-5">
                <span class="text-120 text-secondary-d1">$${totalAmount}</span>
            </div>
        </div>

        <div class="row my-2">
            <div class="col-7 text-right">
                Delivery charge: 
            </div>
            <div class="col-5">
                <span class="text-110 text-secondary-d1">Waived</span>
            </div>
        </div>

        <div class="row my-2 align-items-center bgc-primary-l3 p-2">
            <div class="col-7 text-right">
                Total Amount
            </div>
            <div class="col-5">
                <span class="text-150 text-success-d3 opacity-2">$${totalAmount}</span>
            </div>
        </div>
    </div>
</div>
`

    totalamountContainer.innerHTML = totoalAmountItemBox;   

}

function removeOrderInfoFromDOM() {
    orderHistoryList.innerHTML = ""

}

function showUserInfo(users) {
    console.log(users)
    const itemBoxUserInfo = `
    <div>
        <span class="text-sm text-grey-m2 align-middle">To:</span>
        <span class="text-600 text-110 text-blue align-middle">${users.first_name} ${users.last_name}</span>
    </div>
    <div class="text-grey-m2">
        <div class="my-1">
            ${users.shipping_address}
        </div>
        <div class="my-1"><i class="fa-solid fa-envelope" text-secondary"></i> <b class="text-600">${users.email}</b>
        </div>
    </div>
`
    userInfoList.innerHTML = itemBoxUserInfo;
}

function removeUserInfoFromDOM() {
    userInfoList.innerHTML = ""

}
