// const { isConstructorDeclaration } = require("typescript")

// Example starter JavaScript for disabling form submissions if there are invalid fields
(function () {
    'use strict'

    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.querySelectorAll('.needs-validation')

    // Loop over them and prevent submission
    Array.prototype.slice.call(forms)
        .forEach(function (form) {
            form.addEventListener('submit', function (event) {
                if (!form.checkValidity()) {
                    event.preventDefault()
                    event.stopPropagation()
                }

                form.classList.add('was-validated')
            }, false)
        })
})()

let cartItemBox = `
    <li class="list-group-item d-flex justify-content-between lh-sm">
    <div>
    <h6 class="my-0">Product name</h6>
    <small class="text-muted">Brief description</small>
    </div>
    <span class="text-muted">$12</span>
    </li>
    `
// let 



// ------------------------------PAYMENT----------------------------------

const checkoutBtn = document.querySelector("#checkoutBtn")



checkoutBtn.addEventListener("click", () => {
  // let session = fetch(`/session`, {
  //   method:"GET"
  // })
  // .then(res => res.json())
  // .catch(err => console.error(err))

  // let user = session.json()
  // // console.log(JSON.stringify(user));
  // console.log(user);


  fetch("https://pet-shop.loca.lt/create-checkout-session", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      items: JSON.stringify(localStorage.getItem('cartLocal'))
    }),
  })
    .then(res => {
      if (res.ok) return res.json()
      return res.json().then(json => Promise.reject(json))
    })
    .then(({ url }) => {
        // console.log(url)
      window.location = url
    })
    .catch(err => {
      console.error(err.error)
    })
})

