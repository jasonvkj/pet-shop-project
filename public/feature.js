let productList = document.querySelector('.product-list')
// let productTemplate = productList.querySelector('.product')
let listDogFeature = document.getElementById('dogFeature')
let listCatFeature = document.getElementById('catFeature')

function addToCart(id){
    // finished version! LAST UPDATE AT 13/5/2022 BY PAUL
    
    console.log("Product ID: ", id, " added to cart...")

    var retrievedCart = localStorage.getItem('cartLocal');
                    
            if(retrievedCart === null || retrievedCart === undefined) {
                // new item added to cart(in local storage)
                var newItem = [{"productId": parseInt(id), "cart_qty": 1}];
                newItemString = JSON.stringify(newItem)
                
                localStorage.setItem('cartLocal', newItemString);
                
                newItemJson = JSON.parse(newItemString)
                retrievedCart = newItemJson;
                console.log("new item created")
            } 
            else {
                let ExistedCart = JSON.parse(retrievedCart);
                //{"productId": parseInt(id), "cart_qty": 1}

                let found = false;
                for(let i in ExistedCart){
                    console.log(i);
                    if(ExistedCart[i].productId == id){
                        ExistedCart[i].cart_qty ++;
                        found = true;
                        break;
                    }
                }

                if(!found){
                    ExistedCart.push({"productId": id, "cart_qty": 1})
                }

                localStorage.setItem('cartLocal', JSON.stringify(ExistedCart))
            }           
}

var clicked = false;

function happySave() {
    if (!clicked) {
        clicked = true;
        Swal.fire({
            position: 'center',
            // icon: 'success',
            // background: 'yellow',
            title: 'Saved in your wish list🤗',
            showConfirmButton: false,
            timer: 1100
          })
        clicked = false;
    }
}

function sadDrop() {
    if (!clicked) {
        clicked = true;
        Swal.fire({
            position: 'center',
            // icon: ':(',
            title: 'What a pity 🥺',
            showConfirmButton: false,
            timer: 800
          })
        clicked = false;
    }
}

function addToWishList(id) {
    fetch(`/product/${id}/like`, { method: 'POST' })
        .then(res => res.json())
        .catch(err => ({ error: String(err) }))
        .then(json => {
            if (json.error) {
                // alert(json.error) // this can be blocked

                Swal.fire({
                    icon: 'error',
                    title: 'Log-in is required.',
                    text: json.error,
                })
                return
            }
            //   errorMessage.hidden = true
            happySave();
            console.log("Show like-reminder")
                })
    console.log("Added to wishList successfully")
}

function deleteFromWishList(id) {
    fetch(`/product/${id}/like`, { method: 'DELETE' })
        .then(res => res.json())
        .catch(err => ({ error: String(err) }))
        .then(json => {
            // debugger
            if (json.error) {
                // alert(json.error) // this can be blocked

                Swal.fire({
                    icon: 'error',
                    title: 'Log-in is required.',
                    text: json.error,
                })
                return
            }
            // errorMessage.hidden = true
            sadDrop();
            console.log("Show unlike-reminder")
        })
    console.log("Deleted from wishList successfully")
}

// productTemplate.remove()

fetch('/feature/')
    .then(res => res.json())
    .catch(error => ({ error: String(error) }))
    .then(json => {
        if (json.error) {
            Swal.fire({
                icon: 'error',
                title: 'Failed to load products',
                text: json.error,
            })
            return
        }
        json.forEach(showProduct)
    })

function removeProductFromDOM(id) {
    let product = productList.querySelector(`.product[data-id="${id}"]`)
    if (product) {
        product.remove()
    }
}

function removeProductListFromDOM() {
    let products = productList.querySelectorAll('.product')
    
    products.forEach( product => {
        product.remove()
    })        
    // why 4 products each time to remove?
}

let isAutoPush = false

function showProduct(product) {
    console.log(product)
    // let productContainer
    // if (isAutoPush) {
    //     // show this product at top
    //     removeProductFromDOM(product.id)
    //     // console.log("removedProduct-id to clone for new")
    //     productContainer = productTemplate.cloneNode(true)
    // } else {
    //     // show this product in original order
    //     productContainer = productList.querySelector(`.product[data-id="${product.id}"]`)
    //     if (productContainer) {
    //         product.mode = 'in-place'
    //     } else {
    //         productContainer = productTemplate.cloneNode(true)
    //     }
    // }

    // productContainer.dataset.id = product.id 

    // // let memoLink = memoContainer.querySelector('.memo-detail-link')
    // // memoLink.href = '/memo.html?id=' + memo.id

    // let productContent = productContainer.querySelector('.product-content')
    // productContent.innerHTML = product.product_name + productContent.innerHTML;

    // let img = productContainer.querySelector('img')
    // // console.log(productContent.textContent)
    // if (product.product_image) {
    //     img.src = "img/" + product.product_image
    // } else {
    //     img.remove()
    // }

    // let productSmall = productContainer.querySelector('small')
    // productSmall.innerHTML = " Price: HKD " + product.price + productSmall.innerHTML;

    // productList.appendChild(productContainer)

    const itemBox = `
    <div class="product" data-id="${product.id}">
    <div class="product-inner">
        <div class="product-content">
        <h6>${product.product_name || ""}</h6>
            <img src="img/${product.product_image || ""}" class="d-block mx-auto" />
            <div class="bar-row">
                <small>Price: HKD ${product.price}</small>
                    <!-- <button class="like-btn" onclick="likeMemo()">💓</button> -->
                    <button onclick="addToWishList(${product.id})"><i class="fa-solid fa-heart"></i>Like</button>
                    <button onclick="deleteFromWishList(${product.id})"><i class="fa-regular fa-heart"></i>Unlike</button>
                    <!-- <button class="unlike-btn" onclick="unlikeMemo()">💔</button> -->
                <button onclick="addToCart(${product.id})">Add to Cart</button>
            </div>
        </div>
    </div>
    </div>
    `

    productList.innerHTML += itemBox;

}
// <---------------------------------------------------------------->

listCatFeature.addEventListener('click', event => {
    event.preventDefault()
    console.log("clicked catFeature")
    fetch('/catFeature')
        .then(res => res.json())
        .catch(error => ({ error: String(error) }))
        .then(json => {
            if (json.error) {
                Swal.fire({
                    icon: 'error',
                    title: 'Failed to load product catFeature',
                    text: json.error,
                })
                return
            }
            json.forEach(removeProductListFromDOM)
            console.log("removed Whole List")
            json.forEach(showProduct)
            console.log("loaded catFeature")
        })

})

listDogFeature.addEventListener('click', event => {
    event.preventDefault()
    console.log("clicked dogFeature")
    fetch('/dogFeature')
        .then(res => res.json())
        .catch(error => ({ error: String(error) }))
        .then(json => {
            if (json.error) {
                Swal.fire({
                    icon: 'error',
                    title: 'Failed to load product dogFeature',
                    text: json.error,
                })
                return
            }
            json.forEach(removeProductListFromDOM)
            console.log("removed Whole List")
            json.forEach(showProduct)
            console.log("loaded dogFeature")
        })

})

