let accountDetails = document.getElementById('#account-details')
let paymentDetails = document.getElementById('#payment-details')
let orderHistory = document.getElementById('#order-history')
let securityDetails = document.getElementById('#security-settings')

// let inputAddress = document.getElementById('#inputAddress')
// let inputAddress2 = document.getElementById('#inputAddress2')
// let inputDistrict = document.getElementById('#inputDistrict')
// let inputRegion = document.getElementById('#inputRegion')
// let inputCountry = document.getElementById('#inputCountry')

// let inputBillingAddress = document.getElementById('#inputBillingAddress')
// let inputBillingAddress2 = document.getElementById('#inputBillingAddress2')
// let inputBillingDistrict = document.getElementById('#inputBillingDistrict')
// let inputBillingRegion = document.getElementById('#inputBillingRegion')
// let inputBillingCountry = document.getElementById('#inputBillingCountry')



// console.log(districtValue.value)
// console.log(inputAddress.value + inputAddress2.value)

let currentUser = document.querySelector('#current-username');
let logoutForm = document.querySelector('#logout-form');
let accountForm = document.querySelector('#account-form')
let changePasswordForm = document.querySelector('#change-password-form')
let deleteForm = document.querySelector('#delete-form')
let updateCheck = document.querySelector('#update-check')
let pwUpdateCheck = document.querySelector('#pw-update-check')

let user_id = null;

fetch('/session')
    .then(res => res.json())
    .catch(error => ({ error: String(error) }))
    .then(json => {
        if (json.error) {
            let message = "Error, please try again."
            console.error(message, json.error)
        }
        setUserId(json.id)
        console.log(json.id)
        currentUser.textContent = json.username
    })

function addUserStyle() {
    let link = document.createElement('link')
    link.id = 'user-style'
    link.rel = 'stylesheet'
    link.href = '/user/user.css'
    document.head.appendChild(link)
}

function removeUserStyle() {
    let link = document.querySelector('#user-style')
    if (link) {
        link.remove()
    }
}
function setUserId(id) {
    user_id = id
    if (user_id) {
        addUserStyle()
    } else {
        removeUserStyle()
    }
    // reconnectSocketIO()
}

// Front end buttons functions 
let personalSettingsContainer = document.querySelector('#personal-details')
let orderHistoryContainer = document.querySelector('#orderHistoryContainer')
let shippingAddressContainer = document.querySelector("#shippingAddressContainer")
let checkBoxContainer = document.querySelector('#checkBoxSelector')
let changePasswordContainer = document.querySelector("#security-settings")
let formButtons = document.querySelector("#form-buttons")

let addressCheckBox = document.getElementById('flexCheckDefault')
let billingAddressContainer = document.querySelector("#billingAddressContainer")

//// Check Box for Shipping & Billing Address
function checkbox() {
    addressCheckBox.addEventListener("change", (event) => {
        if (event.target.checked) {
            billingAddressContainer.classList.add("billingAddressContainerToggle");
        } else {
            billingAddressContainer.classList.remove("billingAddressContainerToggle");

        }
    });
}

//// Hide and Show elements
function personalInfoButton() {
    personalSettingsContainer.style.display = "block"
    orderHistoryContainer.style.display = "none"
    shippingAddressContainer.style.display = "none"
    checkBoxContainer.style.display = "none"
    billingAddressContainer.style.display = "none"
    changePasswordContainer.style.display = "none"
    formButtons.style.display = "block"
    console.log("personal div clicked")
    // return
}

function orderHistoryMenu() {
    personalSettingsContainer.style.display = "none"
    orderHistoryContainer.style.display = "block"
    shippingAddressContainer.style.display = "none"
    checkBoxContainer.style.display = "none"
    formButtons.style.display = "none!important"
    billingAddressContainer.style.display = "none"
    changePasswordContainer.style.display = "none"
    console.log("order history div clicked")

}

function shippingBillingButton() {
    personalSettingsContainer.style.display = "none"
    orderHistoryContainer.style.display = "none"
    shippingAddressContainer.style.display = "block"
    checkBoxContainer.style.display = "block"
    billingAddressContainer.style.display = "block"
    changePasswordContainer.style.display = "none"
    formButtons.style.display = "block"
    console.log("ship bill div clicked")
}
 
function securityButton() {
    personalSettingsContainer.style.display = "none"
    orderHistoryContainer.style.display = "none"
    shippingAddressContainer.style.display = "none"
    checkBoxContainer.style.display = "none"
    billingAddressContainer.style.display = "none"
    changePasswordContainer.style.display = "block"
    formButtons.style.display = "none"
    console.log("change pw div clicked")
}


function ajaxForm(options) {
    const { form, getBody, cb, method = form.method } = options
    form.addEventListener('submit', event => {
        console.log(form.method)
        event.preventDefault()
        Promise.resolve(getBody)
            .then(getBody => JSON.stringify(getBody()))
            .then(body =>
                //     {
                //     console.log(body);
                //     return body;
                // }
                fetch(form.action, {
                    method: method,
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body,
                }),
            )
            .then(res => res.json())
            .catch(error => ({ error: String(error) }))
            .then(cb)
    })
}

ajaxForm({
    form: accountForm,
    getBody() {
        var completeShippingAddress = [inputAddress.value,
        inputAddress2.value,
        inputDistrict.value,
        inputRegion.value,
        inputCountry.value]

        var completeBillingAddress = [inputBillingAddress.value,
        inputBillingAddress2.value,
        inputBillingDistrict.value,
        inputBillingRegion.value,
        inputBillingCountry.value]

        var completeBillingAddress2 = []

        //  test copy function Shipping Same as Billing
        // console.log("billing address 2 brought forward: ", completeBillingAddress2)

        if (addressCheckBox.checked) {
            console.log(addressCheckBox.checked)
            for (let i = 0; i < completeShippingAddress.length; i++) {
                completeBillingAddress2.push(completeShippingAddress[i])

            }
            // completeBillingAddress2=[...completeShippingAddress]

            console.log(completeBillingAddress2)
            return {
                firstName: accountForm.firstName.value,
                lastName: accountForm.lastName.value,
                email: accountForm.email.value,
                date_of_birth: accountForm.dateOfBirth.value,
                shipping_address: completeShippingAddress.join(', '),
                billing_address: completeBillingAddress2.join(', ')
            }
        } else {
            return {
                firstName: accountForm.firstName.value,
                lastName: accountForm.lastName.value,
                email: accountForm.email.value,
                date_of_birth: accountForm.dateOfBirth.value,
                shipping_address: completeShippingAddress.join(', '),
                billing_address: completeBillingAddress.join(', ')
            }
        }
        // return addressCheckBox.check?{
        //     ...res,
        //     billing_address: completeBillingAddress
        // }:
        // {
        //     ...res,
        //     shipping_address: completeShippingAddress
        // }
    },
    cb: json => {
        if (json.error) {
            // console.log(value)
            console.error('Error update Ajax Form: ', json.error)
            return
        }
        updateCheck.textContent = "Successfully updated."
    },
})


//Update Password Form

function updatePassword() {
    if (changePasswordForm.inputNewPassword2.value !== changePasswordForm.inputNewPassword.value) {
        pwUpdateCheck.textContent = 'new password not matched.'
        return
    }
    ajaxForm({
        form: changePasswordForm,
        getBody() {
            return {
                currentPassword: changePasswordForm.inputCurrentPassword.value,
                newPassword: changePasswordForm.inputNewPassword.value,
            }
        },
        cb: json => {

            if (json.error) {
                // console.log(value)
                console.error('Error Password update Ajax Form: ', json.error)

                pwUpdateCheck.textContent = "Update failed. Please try again."
                return
            }
            pwUpdateCheck.textContent = "Successfully updated."
        },
    })


}

ajaxForm({
    form: deleteForm,
    getBody() {
        return {
            // username: deleteForm.deleteUsername.value,
            password: deleteForm.deleteUserPassword.value
        }
    },
    cb: json => {
        if (json.error) {
            console.log(json)
            //console.error('Error delete Ajax Form: ', json.error)
            return
        }
        if (json.success) {
            window.location = "/";
        }
        setUserId(null)
        updateCheck.textContent = "Successfully deleted."
    },
    method: "delete"
})



// ORDER HISTORY GET