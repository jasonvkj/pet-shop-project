        <div class="row">
            <div class="col-md-10 col-11 mx-auto">
                <div class="row mt-5 gx-3">
                    <!-- left side div -->
                    <div class="col-md-12 col-lg-8 col-11 mx-auto main_cart mb-lg-0 mb-5 shadow">
                        <div class="card p-4">
                            <h2 class="py-4 font-weight-bold">Shopping Cart</h2>


                    <!-- right side div -->
                    <div class="col-md-12 col-lg-4 col-11 mx-auto mt-lg-0 mt-md-5">
                        <div class="right_side p-3 shadow bg-white">
                            <h2 class="product_name font-weight-bold mb-5">Total</h2>
                            <div class="price_indiv d-flex justify-content-between">
                                <p>Product amount</p>
                                <p>$<span id="product_total_amt">0.00</span></p>
                            </div>
                            <div class="price_indiv d-flex justify-content-between">
                                <p>Shipping Charge</p>
                                <p>$<span id="shipping_charge">50.0</span></p>
                            </div>
                            <hr />
                            <div class="total-amt d-flex justify-content-between font-weight-bold">
                                <p>Total</p>
                                <p>$<span id="total_cart_amt">0.00</span></p>
                            </div>
                            <button class="btn btn-primary text-uppercase">Checkout</button>
                        </div>

                        <div class="mt-3 shadow p-3 bg-white">
                            <div class="pt-4">
                                <h5 class="mb-4">Expected delivery date</h5>
                                <p>April 30th 2022 - May 16th 2022</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>