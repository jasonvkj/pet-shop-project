import express from "express";
import { client } from "./database";
import { catchError } from "./error";
import { userGuard } from "./guard";

export let productRoutes = express.Router();

productRoutes.get("/product", (req, res) => {
  client
    .query(
      /* sql */ `
select id,product_name, product_image,price,productType_id,petType_id,brand_id from product
where petType_id =2
order by id asc
`
    )
    .then((result) => {
      res.json(result.rows);
    })
    .catch((error) => {
      res.status(500).json({ error: String(error) });
    });
});

productRoutes.get("/catPage", (req, res) => {
  client
    .query(
      /* sql */ `
select id,product_name, product_image,price,productType_id,petType_id,brand_id from product 
where petType_id =2
order by id asc
`
    )
    .then((result) => {
      res.json(result.rows);
    })
    .catch((error) => {
      res.status(500).json({ error: String(error) });
    });
});

productRoutes.get("/dogPage", (req, res) => {
  client
    .query(
      /* sql */ `
select id,product_name, product_image,price,productType_id,petType_id,brand_id from product 
where petType_id =1
order by id asc
`
    )
    .then((result) => {
      res.json(result.rows);
    })
    .catch((error) => {
      res.status(500).json({ error: String(error) });
    });
});

// <---------------------------------------------------------------->
productRoutes.get("/feature", (req, res) => {
  client
    .query(
      /* sql */ `
select id,product_name, product_image,price,productType_id,petType_id,brand_id,updated_at from product 
where (id > 9 and petType_id =2) OR (id > 11 and petType_id =1);
`
    )
    .then((result) => {
      res.json(result.rows);
    })
    .catch((error) => {
      res.status(500).json({ error: String(error) });
    });
});

productRoutes.get("/catFeature", (req, res) => {
  client
    .query(
      /* sql */ `
select id,product_name, product_image,price,productType_id,petType_id,brand_id,updated_at from product 
where id > 9 and petType_id = 2;
`
    )
    .then((result) => {
      res.json(result.rows);
    })
    .catch((error) => {
      res.status(500).json({ error: String(error) });
    });
});

productRoutes.get("/dogFeature", (req, res) => {
  client
    .query(
      /* sql */ `
select id,product_name, product_image,price,productType_id,petType_id,brand_id,updated_at from product 
where id > 11 and petType_id = 1;
`
    )
    .then((result) => {
      res.json(result.rows);
    })
    .catch((error) => {
      res.status(500).json({ error: String(error) });
    });
});

productRoutes.get("/hotSales", (req, res) => {
  client
    .query(
      /* sql */ `
select id,product_name, product_image,price,productType_id,petType_id,brand_id from product
where exists(select product_id, sum(quantity) from productHistory
where product_id=product.id group by product_id having SUM(quantity) > 5);
`
    )
    .then((result) => {
      res.json(result.rows);
    })
    .catch((error) => {
      res.status(500).json({ error: String(error) });
    });
});

productRoutes.get("/catHotSales", (req, res) => {
  client
    .query(
      /* sql */ `
select id,product_name, product_image,price,productType_id,petType_id,brand_id from product
where exists(select product_id, petType_id, sum(quantity) from productHistory
where product_id=product.id group by product_id having SUM(quantity) > 5 AND petType_id =2);
`
    )
    .then((result) => {
      res.json(result.rows);
    })
    .catch((error) => {
      res.status(500).json({ error: String(error) });
    });
});

productRoutes.get("/dogHotSales", (req, res) => {
  client
    .query(
      /* sql */ `
select id,product_name, product_image,price,productType_id,petType_id,brand_id from product
where exists(select product_id, petType_id, sum(quantity) from productHistory
where product_id=product.id group by product_id having SUM(quantity) > 5 AND petType_id =1);
`
    )
    .then((result) => {
      res.json(result.rows);
    })
    .catch((error) => {
      res.status(500).json({ error: String(error) });
    });
});

// <-----------------Wishlist --------------------------->

productRoutes.post("/product/:id/like", userGuard, (req, res) => {
  let user_id = req.session.user?.id;
  let product_id = +req.params.id;
  if (!product_id) {
    res.status(400).json({ error: "Missing id in req.params" });
    return;
  }

  client
    .query(
      /* sql */ `
insert into wishList
(user_id, product_id, product_name)
values
($1, $2,(select product_name from product where id = $2))
`,
      [user_id, product_id]
    )
    .catch((error) => {
      error = String(error);
      if (error.includes("unique")) {
        return;
      }
      throw error;
    })
    .then(() => {
      res.json({ ok: true });
      // io.emit('new-liker', req.session.user)
    })
    .catch((error) => {
      res.status(500).json({ error: String(error) });
    });
});

productRoutes.delete("/product/:id/like", userGuard, (req, res) => {
  let user_id = req.session.user?.id;
  let product_id = +req.params.id;
  if (!product_id) {
    res.status(400).json({ error: "Missing id in req.params" });
    return;
  }
  client
    .query(
      /* sql */ `
delete from wishList
where user_id = $1
  and product_id = $2
`,
      [user_id, product_id]
    )
    .then(() => {
      res.json({ ok: true });
      // io.emit('liker left', req.session.user)
    })
    .catch((error) => {
      res.status(500).json({ error: String(error) });
    });
});

// <---------------------ListSavedItem-------------------------------------->
productRoutes.get("/listSavedItem/:id", userGuard, (req, res) => {
  let user_id = req.session.user?.id;
  // let product_id = +req.params.id
  // if (!product_id) {
  //   res.status(400).json({ error: 'Missing id in req.params' })
  //   return
  // }
  client
    .query(
      /* sql */ `
      select id,product_name, product_image,price,productType_id,petType_id,brand_id from product
      where exists(select product_id, user_id from wishList
      where product_id=product.id AND user_id = $1);
`,
      [user_id]
    )
    .then((result) => {
      res.json(result.rows);
    })
    .catch((error) => {
      res.status(500).json({ error: String(error) });
    });
});
// <-----------------------ListingWhatOrderNoForLogInUser----------------------------------------->
//Should be orderRouter bcoz from Table order, nth with product
productRoutes.get("/orderNo/:id", userGuard, (req, res) => {
  let user_id = req.session.user?.id;
  // let product_id = +req.params.id
  // if (!product_id) {
  //   res.status(400).json({ error: 'Missing id in req.params' })
  //   return
  // }
  client
    .query(
      /* sql */ `
      select id, user_id, status, created_at::TIMESTAMP::DATE from "order"
      where user_id=$1;
`,
      [user_id]
    )
    .then((result) => {
      res.json(result.rows);
    })
    .catch((error) => {
      res.status(500).json({ error: String(error) });
    });
});

// <-----------------ListingItemsFromProductHistoryBasedOnOrderNo---------------------->
productRoutes.get("/order/:id", (req, res) => {
  let order_id = +req.params.id
    if (!order_id) {
         res.status(400).json({ error: 'Missing id in req.params' })
        return
    }
    client
        .query(
        /* sql */ `
  select order_id, product_id, product_name, quantity, product_price, price_total, created_at from productHistory 
  where order_id = $1;
  `,
            [order_id],
        )
        .then(result => {
            res.json(result.rows)
        })
        .catch(error => {
            res.status(500).json({ error: String(error) })
        })
})

productRoutes.get("/order/:id/orderInfo", (req, res) => {
  let id = +req.params.id
    if (!id) {
         res.status(400).json({ error: 'Missing id in req.params' })
        return
    }
    client
        .query(
        /* sql */ `
  select id, created_at, status, order_total from "order" 
  where id = $1;
  `,
            [id],
        )
        .then(result => {
            res.json(result.rows)
        })
        .catch(error => {
            res.status(500).json({ error: String(error) })
        })
})



// SEARCH PRODUCTS WITH GET

// productRoutes.get("/product/:id", async (req, res) => {
//   let keyword = req.body.keyword;
//   console.log(keyword);
//   if (!keyword) {
//     res.status(400).json({ error: "Missing product" });
//     return;
//   }
//   client
//     .query(
//       /*sql*/ `
//     select id, 
//     product_name, 
//     product_image,
//     price,
//     productType_id,
//     petType_id,
//     brand_id 
//     from product
//     where product_name=$1 
//     order by id asc
//     `
//     )
//     .then((result) => {
//       res.json(result.rows);
//       // if(){

//       // }
//     })
//     .catch((error) => {
//       res.status(500).json({ error: String(error) });
//     });
// });



productRoutes.get("/listSavedItem/:id", userGuard, (req, res) => {
  let user_id = req.session.user?.id;
  // let product_id = +req.params.id
  // if (!product_id) {
  //   res.status(400).json({ error: 'Missing id in req.params' })
  //   return
  // }
  client
    .query(
      /* sql */ `
      select id,product_name, product_image,price,productType_id,petType_id,brand_id from product
      where exists(select product_id, user_id from wishList
      where product_id=product.id AND user_id = $1);
`,
      [user_id]
    )
    .then((result) => {
      res.json(result.rows);
    })
    .catch((error) => {
      res.status(500).json({ error: String(error) });
    });
});


productRoutes.get("/listUserInfo/:id", userGuard, (req, res) => {
  let id = req.session.user?.id;
  // let order_id = +req.params.id;
  // let product_id = +req.params.id
  // if (!product_id) {
  //   res.status(400).json({ error: 'Missing id in req.params' })
  //   return
  // }
  client
    .query(
      /* sql */ `
      select id, first_name, last_name, shipping_address, email from users 
      where id = $1;
`,
      [id]
    )
    .then((result) => {
      res.json(result.rows);
    })
    .catch((error) => {
      res.status(500).json({ error: String(error) });
    });
});


productRoutes.get("/order/:id/timestamp", (req, res) => {
  let created_at = +req.params.id
    if (!created_at) {
         res.status(400).json({ error: 'Missing id in req.params' })
        return
    }
    client
        .query(
        /* sql */ `
  select order_id, product_id, product_name, quantity, product_price, price_total, created_at from productHistory 
  where created_at = $1;
  `,
            [created_at],
        )
        .then(result => {
            res.json(result.rows)
        })
        .catch(error => {
            res.status(500).json({ error: String(error) })
        })
})

productRoutes.get("/order/:id/orderInfo/timestamp", (req, res) => {
  let created_at = +req.params.id
    if (!created_at) {
         res.status(400).json({ error: 'Missing id in req.params' })
        return
    }
    client
        .query(
        /* sql */ `
  select id, created_at, status from "order" 
  where created_at = $1;
  `,
            [created_at],
        )
        .then(result => {
            res.json(result.rows)
        })
        .catch(error => {
            res.status(500).json({ error: String(error) })
        })
})

