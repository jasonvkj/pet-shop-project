import { Request, Response, NextFunction } from 'express'
import './session'

export let userGuard = (req: Request, res: Response, next: NextFunction) => {
  if (req.session?.user) {
    next()
  } else {
    res
      .status(401)
      .json({ error: 'This is only accessible by pet-shop member' })
  }
}

export let adminGuard = (req: Request, res: Response, next: NextFunction) =>{
  if (req.session?.user) {
    next()
  } else {
    res
      .status(401)
      .json({ error: 'This is only accessible by pet-shop member' })
  }
}