import grant from 'grant'
import { env } from './env'

console.log('grant origin:', env.ORIGIN)

export let grantMiddleware = grant.express({
  defaults: {
    origin: env.ORIGIN,
    transport: 'session',
    state: true,
  },
  google: {
    key: env.GOOGLE_CLIENT_ID,
    secret: env.GOOGLE_CLIENT_SECRET,
    scope: ['email'],
    callback: '/login/google',
  },
})
