import { client } from './database'
import { hashPassword } from './hash'

async function main() {
  let result = await client.query(/* sql */ `
select id, password
from users
where password_hash is null
`)
  for (let user of result.rows) {
    let password_hash = await hashPassword(user.password)
    await client.query(
      /* sql */ `
update users
set password_hash = $1
  , password = null
where id = $2
`,
      [password_hash, user.id],
    )
  }
  await client.end()
  console.log('finished update')
}
main()
