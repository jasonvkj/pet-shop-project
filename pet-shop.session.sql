create database "pet-shop";
\ c "pet-shop";
create table users (
	id serial primary key,
	is_admin boolean default false,
	username varchar(128),
	password varchar(255),
	password_hash char(60),
	first_name varchar(64),
	last_name varchar(64),
	email varchar(255),
	date_of_birth date,
	created_at timestamp,
	updated_at timestamp,
	shipping_address varchar(255),
	billing_address varchar(255)
);

create table petType (
	id serial primary key,
	petType_name varchar(128),
	petType_image varchar(255),
	created_at timestamp default current_timestamp,
	updated_at timestamp
);
create table productType (
	id serial primary key,
	productType_name varchar(128),
	productType_image varchar(255),
	created_at timestamp default current_timestamp,
	updated_at timestamp
);
create table brand (
	id serial primary key,
	brand_name varchar(128),
	brand_image varchar(255),
	created_at timestamp default current_timestamp,
	updated_at timestamp
);
create table product (
	id serial primary key,
	petType_id INT,
	FOREIGN KEY (petType_id) REFERENCES petType(id),
	productType_id INT,
	FOREIGN KEY (productType_id) REFERENCES productType(id),
	brand_id INT,
	FOREIGN KEY (brand_id) REFERENCES brand(id),
	product_name varchar(128),
	price INT,
	stock_level INT,
	product_image varchar(255),
	created_at timestamp default current_timestamp,
	updated_at timestamp
);
create table orderList (
	id serial primary key,
	user_id INT,
	FOREIGN KEY (user_id) REFERENCES users (id),
	created_at TIMESTAMP,
	quantity INT,
	order_total INT
);

create table productHistory (
	id serial primary key,
	orderList_id INT,
	FOREIGN KEY (orderList_id) REFERENCES orderList(id),
	product_id INT,
	product_name varchar(128),
	product_price INT,
	quantity INT,
	price_total INT,
	created_at TIMESTAMP
);
create table wishList (
	id serial primary key,
	user_id INT,
	FOREIGN KEY (user_id) REFERENCES users (id),
	product_id INT,
	FOREIGN KEY (product_id) REFERENCES product(id),
	product_name varchar(128)
);
create table shoppingCart (
	id serial primary key,
	user_id INT,
	FOREIGN KEY (user_id) REFERENCES users (id),
	product_id INT,
	FOREIGN KEY (product_id) REFERENCES product(id),
	quantity INT,
	price_total INT,
	created_at timestamp,
	updated_at timestamp,
	shipping_id INT,
	shipping_address varchar(255),
	shipping_at timestamp
);


-- ALTER TABLE wishList DROP COLUMN quantity;
-- ALTER TABLE wiproductHistory DROP COLUMN user_id;
-- ALTER TABLE productHistory RENAME COLUMN date_of_purchase TO created_at;
-- ALTER TABLE orderList RENAME COLUMN date_of_purchase TO created_at;
-- ALTER TABLE "user" RENAME COLUMN "user_name" TO "username";
-- ALTER TABLE "product" RENAME COLUMN brandType_id TO brand_id;
-- ALTER TABLE product RENAME COLUMN 
-- TRUNCATE TABLE product, pettype, productType, brand, "user", orderList, productHistory, wishList, shoppingCart;
-- ALTER TABLE shoppingCart 
-- ALTER TABLE shoppingCart
--     ADD CONSTRAINT fk_shoppingCart_user_id FOREIGN KEY (user_id) REFERENCES user(id);
-- (if u wanna add fk to existing table column)


-- SQL TABLE UPDATE ADVISED BY BEENO / STRIPE FINSISHED (13/5/2022)
alter table orderlist rename to "order";
alter table "order" drop COLUMN quantity;
CREATE TYPE order_status AS ENUM ('pending','paid', 'cancel');
alter table "order" add COLUMN status order_status;

alter table producthistory rename column orderlist_id to order_id;

UPDATE "order"
SET status = 'paid'



