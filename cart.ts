import express from 'express'
import { client } from './database'

export let cartRoutes = express.Router()

cartRoutes.get('/product/:id', async (req, res) => {
    try {
        let id = +req.params.id
        console.log("id: ",id)
        if (!id) {
            res.status(400).json({ error: 'Missing id in req.params' })
            return
        }
    
        let result = await client
            .query(
            /* sql */ `
            select
            product_name,
            price,
            product_image
            from product
            where id = $1;
      `, [id],
            )
        let product = result.rows[0]
        console.log(product)
        res.json(product)
    }catch (err: any) {
        res.status(500).json({ error: err.message })
        console.log(err)
    }

})


cartRoutes.get('/checkuser', async (req, res) => {
    if (!req.session.user){
        res.json({ login : false });
    } else {
        res.json({ login: true});
    }
})


cartRoutes.get('/checkuserid', async (req, res) => {
    if (!req.session.user){
        res.json({ login : false });
    } else {
        res.json({ result: req.session.user.id});
    }
})

cartRoutes.get('/finduserdata', async (req, res) => {
    try{
    if (!req.session.user){
        res.status(500).json({ error: "you must be logged in"})
    } else {
        let user = req.session.user 
        let user_id :any = user.id;

        let result = await client.query(
            `select 
            first_name, last_name, email, shipping_address
            from 
            users
            where 
            id = $1`
            , [user_id])
        
        let sqlResult = result.rows[0]
        res.json({sqlResult});
    }
    }catch (err: any){
        res.status(500).json({ error: err.message })
        console.log(err)
    }
})