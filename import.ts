// import { Client } from 'pg'
import { client } from './database'
import dotenv from 'dotenv'
import xlsx from 'xlsx'

dotenv.config()

// console.log('connecting to database:', {
//   database: process.env.DB_NAME,
//   user: process.env.DB_USERNAME,
//   password: process.env.DB_PASSWORD,
// })

// export const client = new Client({
//   database: process.env.DB_NAME,
//   user: process.env.DB_USERNAME,
//   password: process.env.DB_PASSWORD,
// })

let workbook = xlsx.readFile('wsp-raw-excel.xlsx')

type User = {
  id: number
  is_admin: boolean
  username: string
  password: string
  password_hash: string
  first_name: string
  last_name: string
  email: string
  date_of_birth: Date
  created_at: Date
  updated_at: Date
  shipping_address: string
  billing_address: string
}

// should start with capital letter / Pascal naming conventions
type product = {
  id: number
  petType_id: number
  productType_id: number
  brand_id: number
  product_name: string
  price: number
  stock_level: number
  product_image: string
  created_at: Date
  updated_at: Date
}

type petType = {
  id: number
  petType_name: string
  petType_image: string
}

type productType = {
  id: number
  productType_name: string
  productType_image: string
}

type brand = {
  id: number
  brand_name: string
  brand_image: string
  created_at: Date
  updated_at: Date
}

type orderList = {
  id: number
  user_id: number
  created_at: Date
  quantity: number
}

type productHistory = {
  id: number
  orderList_id: number
  product_id: number
  product_name: string
  product_price: number
  quantity: number
  price_total: number
  created_at: Date
}

type wishList = {
  id: number
  user_id: number
  product_id: number
  product_name: string
}

type shoppingCart = {
  id: number
  user_id: number
  product_id: number
  quantity: number
  price_total: number
  created_at: Date
  updated_at: Date
  shipping_id: number
  shipping_address: string
  shipping_at: Date
}

let orderLists = xlsx.utils.sheet_to_json<orderList>(workbook.Sheets.orderList)
let petTypes = xlsx.utils.sheet_to_json<petType>(workbook.Sheets.petType)
let products = xlsx.utils.sheet_to_json<product>(workbook.Sheets.product)
let productHistories = xlsx.utils.sheet_to_json<productHistory>(workbook.Sheets.productHistory)
let productTypes = xlsx.utils.sheet_to_json<productType>(workbook.Sheets.productType)
let shoppingCarts = xlsx.utils.sheet_to_json<shoppingCart>(workbook.Sheets.shoppingCart)
let wishLists = xlsx.utils.sheet_to_json<wishList>(workbook.Sheets.wishList)
let brands = xlsx.utils.sheet_to_json<brand>(workbook.Sheets.brand)
let users = xlsx.utils.sheet_to_json<User>(workbook.Sheets.user)

async function uploadExcel() {
  await client.connect(
    () => { console.log('database connection established') }
  ) // "dial-in" to the postgres server

  for (let user of users) {
    let result = await client.query(
      'SELECT count(*) as is_exist from users where username = $1',
      [user.username],
    )
    if (!+result.rows[0].is_exist) {
      console.log('import user:', user)
      await client.query(
        'INSERT INTO users (id, is_admin, username,password,password_hash,first_name, last_name, email,date_of_birth,created_at,updated_at,shipping_address,billing_address) values ($1,$2,$3,$4,$5,$6,$7,$8,$9,now(),now(),$10,$11)',
        [user.id, user.is_admin, user.username, user.password, user.password_hash, user.first_name, user.last_name, user.email, user.date_of_birth, user.shipping_address, user.billing_address],
      )
    } else {
      console.log('skip user:', user.username)
    }
  }

  for (let petType of petTypes) {
    let result = await client.query(
      'SELECT count(*) as is_exist from petType where petType_name = $1',
      [petType.petType_name],
    )
    if (!+result.rows[0].is_exist) {
      console.log('import petType:', petType)
      await client.query(
        'INSERT INTO petType (petType_name,petType_image) values ($1,$2)',
        [petType.petType_name, petType.petType_image],
      )
    } else {
      console.log('skip petType:', petType.petType_name)
    }
  }

  for (let productType of productTypes) {
    let result = await client.query(
      'SELECT count(*) as is_exist from productType where productType_name = $1',
      [productType.productType_name],
    )
    if (!+result.rows[0].is_exist) {
      console.log('import productType:', productType)
      await client.query(
        'INSERT INTO productType (productType_name,productType_image) values ($1,$2)',
        [productType.productType_name, productType.productType_image],
      )
    } else {
      console.log('skip productType:', productType.productType_name)
    }
  }

  for (let brand of brands) {
    let result = await client.query(
      'SELECT count(*) as is_exist from brand where brand_name = $1',
      [brand.brand_name],
    )
    if (!+result.rows[0].is_exist) {
      console.log('import brand:', brand)
      await client.query(
        'INSERT INTO brand (brand_name,brand_image,created_at,updated_at) values ($1,$2,now(),now())',
        [brand.brand_name, brand.brand_image],
      )
    } else {
      console.log('skip brand:', brand.brand_name)
    }
  }

  for (let product of products) {
    console.log('import product:', product)
    await client.query(
      'INSERT INTO product (petType_id,productType_id,brand_id,product_name,price,stock_level,product_image,created_at,updated_at) values ($1,$2,$3,$4,$5,$6,$7,now(),now())',
      [product.petType_id, product.productType_id, product.brand_id, product.product_name, product.price, product.stock_level, product.product_image],
    )
  }

  for (let orderList of orderLists) {

    console.log('import orderList:', orderList)
    await client.query(
      'INSERT INTO orderList (user_id,created_at,quantity) values ($1,$2,$3)',
      [orderList.user_id, orderList.created_at, orderList.quantity],
    )
  }

  for (let productHistory of productHistories) {

    console.log('import productHistory:', productHistory)
    await client.query(
      'INSERT INTO productHistory (orderList_id,product_id,product_name,product_price,quantity,price_total,created_at) values ($1,$2,$3,$4,$5,$6,$7)',
      [productHistory.orderList_id, productHistory.product_id, productHistory.product_name, productHistory.product_price, productHistory.quantity, productHistory.price_total, productHistory.created_at],
    )
  }

  for (let wishList of wishLists) {

    console.log('import wishList:', wishList)
    await client.query(
      'INSERT INTO wishList (user_id,product_id,product_name) values ($1,$2,$3)',
      [wishList.user_id, wishList.product_id, wishList.product_name],
    )
  }

  for (let shoppingCart of shoppingCarts) {

    console.log('import shoppingCart:', shoppingCart)
    await client.query(
      'INSERT INTO shoppingCart (user_id,product_id,quantity,price_total,created_at,updated_at,shipping_id,shipping_address,shipping_at) values ($1,$2,$3,$4,$5,now(),$6,$7,$8)',
      [shoppingCart.user_id, shoppingCart.product_id, shoppingCart.quantity, shoppingCart.price_total, shoppingCart.created_at, shoppingCart.shipping_id, shoppingCart.shipping_address, shoppingCart.shipping_at],
    )
  }
}

uploadExcel()
  .catch(err => {
    console.error(err)
  })
  .finally(() => {
    client.end() // close connection with the database
  })
