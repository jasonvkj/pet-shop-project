import { config } from 'dotenv'
import populateEnv from 'populate-env'

config()

export let env = {
  DB_NAME: '',
  DB_USER: '',
  DB_PASSWORD: '',
  SESSION_SECRET: '',
  PORT:8080,
  GOOGLE_CLIENT_ID: '',
  GOOGLE_CLIENT_SECRET: '',
  ORIGIN: 'http://localhost:8080'
}

populateEnv(env, { mode: 'halt' })