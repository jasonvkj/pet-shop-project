import express from 'express'
import { client } from './database'

export let brandRoutes = express.Router()

brandRoutes.get('/dogBrand', (req, res) => {
    client
        .query(
        /* sql */ `
  select id, brand_name from brand
  where exists(select brand_id from product where brand_id = brand.id AND petType_id =1)
  order by brand_name asc;
  `,
        )
        .then(result => {
            res.json(result.rows)
        })
        .catch(error => {
            res.status(500).json({ error: String(error) })
        })
})

brandRoutes.get('/catBrand', (req, res) => {
    client
        .query(
        /* sql */ `
  select id, brand_name from brand
  where exists(select brand_id from product where brand_id = brand.id AND petType_id =2)
  order by brand_name asc;
  `,
        )
        .then(result => {
            res.json(result.rows)
        })
        .catch(error => {
            res.status(500).json({ error: String(error) })
        })
})


brandRoutes.get('/brand/:id', (req, res) => {
    let brand_id = +req.params.id
    if (!brand_id) {
         res.status(400).json({ error: 'Missing id in req.params' })
        return
    }
    client
        .query(
        /* sql */ `
  select id,product_name, product_image,price,productType_id,petType_id,brand_id from product 
  where brand_id = $1;
  `,
            [brand_id],
        )
        .then(result => {
            res.json(result.rows)
        })
        .catch(error => {
            res.status(500).json({ error: String(error) })
        })
})