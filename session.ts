import expressSession from 'express-session'
import { GrantSession } from 'grant'
import { env } from './env'

declare module 'express-session' {
    interface SessionData {
        counter?: number
        user?: {
            id: number
            username?: string
            email?: string
            password?: string
        }
        grant?: GrantSession
    }
}

export let sessionMiddleware = expressSession({
    //   secret: env.SESSION_SECRET,
    secret: env.SESSION_SECRET,
    resave: true,
    saveUninitialized: true,
})
