import express from 'express';
import { client } from './database';
import './session'
import { catchError } from './error'
import { hashPassword, isPasswordMatch } from './hash';
import fetch from 'node-fetch';

export let userRoutes = express.Router();

// already used in server.ts
// userRoutes.use(express.urlencoded({ extended: false }))
// userRoutes.use(express.json())

// userRoutes.use(sessionMiddleware);

userRoutes.get('/users', (req, res) => {
    client.query(
        /*sql*/ `
        select * from users`,
    )
        .then(result => {
            res.json(result.rows)
        })
        .catch(err => {
            res.status(500).json({ error: String(err) })
        })
})

//Unused function 

// let nextUserID = 1;

// export function getNextUserID() {
//     let userID = nextUserID;
//     nextUserID++;
//     return userID;
// }


// Unused type

// export type User = {
//     id: number
//     name: string
//     email?: string
//     username?: string
//     password?: string
//     dateOfBirth?: string
//     shipping_address?: string
//     billing_address?: string
//     isAdmin?: boolean // true if admin
// }

userRoutes.post('/signup', async (req, res) => {
    let { username, password } = req.body;
    if (!username) {
        res.status(400).json({ error: 'Missing Username' });
        return
    }
    if (!password) {
        res.status(400).json({ error: 'Missing Password' });
        return
    }
    let result = await client.query('select * from users where username = $1', [username])
    if (result.rows[0]) {
        res.json({ message: 'user already exist!' })
        console.error('This username is already in use. Please try another one.')
        return
    }
    hashPassword(password)
        .then(password_hash =>
            client.query(
    /* sql */ `
    insert into users (username, password_hash, created_at) values ($1,$2,NOW())
    returning id
    `,
                [username, password_hash]
            )
        )
        .then(result => {
            let id = result.rows[0].id
            req.session["user"] = {
                id,
                username,
                password
            }
            res.json({ id })

        })
        // .catch(error => {
        //     const existingUser = (client.query(`SELECT * FROM users WHERE users.username = $1`, [username])).rows;
        //     if (username == existingUser) {
        //         console.log('yay duplicates already')
        //     }
        // })

        .catch(catchError(res))
})

// login user
userRoutes.post('/login', async(req, res) => {
    let { username, password } = req.body;
    if (!username) {
        res.status(400).json({ error: 'Missing Username' });
        return
    }
    if (!password) {
        res.status(400).json({ error: 'Missing Password' });
        return
    }
    client.query(
    /* sql */`
    select id, password_hash from users
    where username = $1
    `,
        [username]
    )
        .then(async result => {
            console.log(result)
            let user = result.rows[0]
            if (!user) {
                res.status(404).json({ error: 'user not found' })
                return
            }
            let { id, password_hash } = user
            console.log(password_hash,password)
            if (!(await isPasswordMatch({ password_hash, password }))) {
                res.status(403).json({ error: 'wrong password' })
                return
            }
            req.session["user"] = {
                id: user.id,
                username,
                email: "",
                password: password
            }
            res.json({ id: user.id })

        })
        .catch(catchError(res))
})


//Login Google OAuth Service
type GoogleProfile = {
    email: string
    picture: string
}

userRoutes.get('/login/google', async (req, res) => {
    console.log('login with google...')
    console.log('session:', req.session)
    let access_token = req.session?.grant?.response?.access_token
    if (!access_token) {
        res.status(400).json({ error: 'missing access_token in grant session' })
        return
    }

    let profile: GoogleProfile
    try {
        let googleRes = await fetch(
            `https://www.googleapis.com/oauth2/v2/userinfo`,
            {
                headers: { Authorization: 'Bearer ' + access_token },
            },
        )
        profile = await googleRes.json()
    } catch (error) {
        res.status(502).json({ error: 'Failed to get user info from Google' })
        return
    }

    try {
        // try to lookup existing users
        let result = await client.query(
        /* sql */ `
  select id from users
  where username = $1
  `,
            [profile.email],
        )
        let user = result.rows[0]

        // auto register if this is a new user
        if (!user) {
            result = await client.query(
          /* sql */ `
  insert into users
  (username) values ($1)
  returning id
  `,
                [profile.email],
            )
            user = result.rows[0]
        }

        let id = user.id
        req.session["user"] = {
            id,
            username: profile.email,
            email: profile.email,
            password: ""
        }
        // res.json({ id })
        res.redirect('/')
    } catch (error) {
        res.status(500).json({ error: 'Database Error: ' + String(error) })
    }
})



userRoutes.post('/logout', (req, res) => {
    req.session.destroy(error => {
        if (error) {
            console.error('failed to destroy session:', error)
        }
        res.json({ role: 'guest' })

        // Add a 2 second timeout page to redirect back to main page
        // res.redirect('/')
    })
})

userRoutes.get('/logout', (req, res) => {
    req.session.destroy(error => {
        if (error) {
            console.error('failed to destroy session:', error)
        }
        //res.json({ role: 'guest' })

        // Add a 2 second timeout page to redirect back to main page
        res.redirect('/')
    })
})

userRoutes.get('/session', (req, res) => {
    if (req.session?.user) {
        res.json(req.session.user)
    } else {
        res.json({})
    }
})

//Update Personal Profile
userRoutes.post('/personal-details', (req, res) => {
    let id = req.session.user?.id
    // let id = +req.params.id
    if (!id) {
        res.status(400).json({ error: 'Missing id in req.params' })
        return
    }
    console.log('req.body:', req.body)

    let first_name = req.body.firstName
    let last_name = req.body.lastName
    let date_of_birth = req.body.date_of_birth
    let email = req.body.email
    let shipping_address = req.body.shipping_address
    let billing_address = req.body.billing_address
    console.log(first_name, last_name, email, date_of_birth, shipping_address, billing_address)

    client.query(
    /*sql*/ `
    update users 
    set 
    first_name = $1,
    last_name = $2,
    email = $3,
    date_of_birth = $4,
    shipping_address = $5,
    billing_address = $6,
    updated_at = NOW()
    where id = $7;
    `,
        [first_name, last_name, email, date_of_birth, shipping_address, billing_address, id]
    )
        .then(result => {
            res.json(result.rows)


        })
        .catch(error => {
            console.error('failed to update user', error)
        })
})

// Change Password
userRoutes.post('/change-password', async (req, res) => {
    let id = req.session.user?.id
    if (!id) {
        res.status(400).json({ error: 'Missing id in req.params' })
        return
    }
    console.log('req.body:', req.body)

    let currentPassword = req.body.currentPassword
    let newPassword = req.body.newPassword

    const users = await client.query(
        /*sql*/`
        select id, password_hash from "users"
        where id = $1
        `,
        [id]
    );
    const user = users.rows[0];
    console.log(id, user, user.password_hash)

    if (!(await isPasswordMatch({ password_hash: user.password_hash, password: currentPassword }))) {
        res.status(403).json({ error: 'Wrong password entered.' })
        return
    }
    if ((await isPasswordMatch({ password_hash: user.password_hash, password: newPassword }))) {
        res.status(403).json({ error: 'Existing password entered.' })
        return
    }

    client.query(
        /*sql*/ `
        update users 
        set 
        password_hash = $1,
        updated_at = NOW()
        where id = $2;
        `,
        [await  hashPassword(newPassword), id]
    )
        .then(result => {
            res.json({ ChangePasswordSuccess: true })
            // res.redirect('/')
        })
        .catch(error => {
            console.log(error)
            res.status(500).json({ error: 'update failed' })
        })
})


//Delete User
userRoutes.delete('/delete-account', async (req, res) => {
    let id = req.session.user?.id
    if (!id) {
        res.status(400).json({ error: 'Missing id in req.params' })
        return
    }
    console.log('req.body:', req.body)

    let username = req.session.user?.username
    let password: any = req.body.password


    const users = await client.query(
        /*sql*/`
        select id, password_hash from users
        where username = $1
        `,
        [username]
    );
    const user = users.rows[0];
    console.log(id, username, user.password_hash,user.password)
    if (!(await isPasswordMatch({ password_hash: user.password_hash, password: password }))) {
        res.status(403).json({ error: 'Wrong password entered.' })
        return
    }
    client
        .query(
/* sql */ `
delete from users
where id = $1 
and username= $2 
and password_hash= $3`,

            [id, username, user.password_hash]
        )
        .then(result => {
            // res.json(result.rows)
            req.session.destroy(error => {
                console.log('user has been deleted.')
                res.json({ success: true })
            });
        })
        .catch(error => {
            res.status(500).json({ error: String(error) })
        })

    //res.json({ role: 'guest' })

})




