import express from 'express'
import { client } from './database'

export let productTypeRoutes = express.Router()

productTypeRoutes.get('/catProductType', (req, res) => {
    client
        .query(
        /* sql */ `
  select id, productType_name from productType
  where exists(select productType_id from product where productType_id = productType.id AND petType_id =2)
  order by productType_name asc;
  `,
        )
        .then(result => {
            res.json(result.rows)
        })
        .catch(error => {
            res.status(500).json({ error: String(error) })
        })
})


productTypeRoutes.get('/catProductType/:id', (req, res) => {
    let productType_id = +req.params.id
    if (!productType_id) {
         res.status(400).json({ error: 'Missing id in req.params' })
        return
    }
    client
        .query(
        /* sql */ `
  select id,product_name, product_image,price,productType_id,petType_id,productType_id from product 
  where productType_id = $1 AND petType_id =2;
  `,
            [productType_id],
        )
        .then(result => {
            res.json(result.rows)
        })
        .catch(error => {
            res.status(500).json({ error: String(error) })
        })
})

productTypeRoutes.get('/dogProductType', (req, res) => {
    client
        .query(
        /* sql */ `
  select id, productType_name from productType
  where exists(select productType_id from product where productType_id = productType.id AND petType_id =1)
  order by productType_name asc;
  `,
        )
        .then(result => {
            res.json(result.rows)
        })
        .catch(error => {
            res.status(500).json({ error: String(error) })
        })
})


productTypeRoutes.get('/dogProductType/:id', (req, res) => {
    let productType_id = +req.params.id
    if (!productType_id) {
         res.status(400).json({ error: 'Missing id in req.params' })
        return
    }
    client
        .query(
        /* sql */ `
  select id,product_name, product_image,price,productType_id,petType_id,productType_id from product 
  where productType_id = $1 AND petType_id =1;
  `,
            [productType_id],
        )
        .then(result => {
            res.json(result.rows)
        })
        .catch(error => {
            res.status(500).json({ error: String(error) })
        })
})