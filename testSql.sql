INSERT INTO 
product 
(product_name,price,stock_level,created_at,updated_at)
VALUES
('test', 999, 999, now(), now())
;

create table product (
	id serial primary key
, petType_id INT
-- ,FOREIGN KEY (petType_id) REFERENCES petType(id)
, productType_id INT
-- ,FOREIGN KEY (productType_id) REFERENCES productType(id)
, brandType_id INT
-- ,FOREIGN KEY (brandType_id) REFERENCES brandType(id)
, product_name varchar(128) 
, price INT
, stock_level INT
, product_image varchar(255)
, created_at timestamp default current_timestamp
, updated_at timestamp
);


ALTER TABLE "user" RENAME COLUMN "user_name" TO "username";