set -e
set -x

npm t
npm run build
scp -r package.json 'jsnw-space:~/myportfolio/'
rsync -SavLP public 'jsnw-space:~/myportfolio/'
rsync -SavLP dist 'jsnw-space:~/myportfolio/'
ssh jsnw-space "
  source ~/.nvm/nvm.sh &&\
  cd ~/myportfolio &&\
  pnpm i --prod &&\
  cd dist &&\
  npx knex migrate:latest &&\
  forever restart ./server.js\
"
