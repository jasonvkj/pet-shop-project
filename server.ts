import express from 'express'
import { print } from 'listening-on'
import { join, resolve } from 'path'
import { cartRoutes } from './cart'
import { client } from './database'
import { grantMiddleware } from './grant'
import { productRoutes } from './product'
import { sessionMiddleware } from './session'
import { brandRoutes } from './brand'
import { productTypeRoutes } from './productType'
import { userRoutes } from './users'
import { stripeRoutes } from './payment'
import { stripeHookRoutes } from './paymenthook'

// TODO: move to env 
export const stripe = require("stripe")('sk_test_51KyUlaDdiwtuqw1vAQVdCWI2ed6FIjxIxgRu1QAwxWp86WfVqYuTEAqAHCt6YXJnjRZi2GoIcdkgmwFLwzjKOEmB00Ci8x9VsU');

// export let ProductRoutes = express.Router()

client.connect(
    (err) => {
        if (err) {
            console.log('database error: ', err);
        }
        console.log('database successfully connected!')
    }
);

export let app = express()

app.use((req, res, next) => {
    console.log(req.method, req.url)
    next()
})



// app.use(env)

// app.use(express.static('public'))
let publicMiddleware = express.static('public')
app.use((req, res, next) => {
    if (req.url.endsWith('.html')) {
        publicMiddleware(req, res, next)
        return
    }
    setTimeout(() => {
        publicMiddleware(req, res, next)
    }, 0)
})

app.use(sessionMiddleware)
app.use(grantMiddleware)

// need to handle raw requests
app.use(stripeHookRoutes)

app.use(express.json())

// for stripe cli webhooks
app.use(express.urlencoded({extended: false}))

// app.use((req, res, next) => {
//     let counter = req.session.counter || 0
//     counter++
//     req.session.counter = counter

//     // this is optional, it should auto save
//     // But for realtime update in concurrent requests, better call the save method explicitly
//     req.session.save()

//     next()
// })
app.use(userRoutes)
app.use(productRoutes)
app.use(brandRoutes)
app.use(productTypeRoutes)

app.use(stripeRoutes)

app.use(cartRoutes)

// app.use(
//     cors({
//       origin: "http://localhost:8080",
//     })
//   )
// app.listen(8080, () => console.log('* Payment Server running on port 8080 *'));


// 404
app.use((req, res) => {
    console.log('404', req.method, req.url)
    res.status(404).sendFile(resolve(join('public', '404.html')))
})

let port = 8080
app.listen(port, () => {
    print(port)
})

